package io.changenow.api.model

/**
 * Created by samosudovd on 07/06/2018.
 */
data class CurrencyItem(
    val id: String,
    val ticker: String,
    val name: String,
    val image: String,
    val isHasExternalId: Boolean,
    val isFiat: Boolean,
    val isAvailable: Boolean
)

data class Estimated(
    val estimatedAmount: Float,
    val networkFee: Float,
    val transactionSpeedForecast: String,
    val warningMessage: String
)

data class FixRateMarket(
    val from: String,
    val to: String,
    val min: Float,
    val max: Float,
    val rate: Float,
    val minerFee: Float
)

data class MinAmount(
    val minAmount: Float
)

data class TxItem(
    val id: String,
    val status: String,
    val hash: String,
    val payinHash: String,
    val payoutHash: String,
    val payinAddress: String,
    val payoutAddress: String,
    val payinExtraId: String,
    val payoutExtraId: String,
    val fromCurrency: String,
    val toCurrency: String,
    val amountSend: String,
    val amountReceive: String,
    val networkFee: String,
    val updatedAt: String,
    val expectedSendAmount: String,
    val expectedReceiveAmount: String,
    val refundAddress: String,
    val refundExtraId: String
)