package io.changenow.api.model.result

sealed class Result<out T: Any> {
    data class Success<T>(val data: T) : Result<Any>()
    data class Error(val exception: Exception) : Result<Nothing>()
}