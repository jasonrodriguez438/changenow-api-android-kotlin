package io.changenow.api.model.result

class CallParameters @JvmOverloads constructor(builder: Builder = Builder()) {
    val isActive: Boolean
    val isFixedRate: Boolean
    val tickerInfo: String?
    val tickerFrom: String?
    val tickerTo: String?
    val txStatus: String?
    val limit: Int
    val offset: Int
    val txId: String?
    val amount: String?
    val address: String?
    val extraId: String?
    val refundAddress: String?
    val refundExtraId: String?
    val userId: String?
    val payload: String?
    val contactEmail: String?
    val isIncludePartners: Boolean
    val dateFrom: String?
    val dateTo: String?

    class Builder {
        var isActive = false
        var fixedRate = false
        var tickerInfo: String? = null
        var tickerFrom: String? = null
        var tickerTo: String? = null
        var txStatus: String? = null
        var limit = 50
        var offset = 0
        var txId: String? = null
        var amount: String? = null
        var address: String? = null
        var extraId: String? = null
        var refundAddress: String? = null
        var refundExtraId: String? = null
        var userId: String? = null
        var payload: String? = null
        var contactEmail: String? = null
        var includePartners = false
        var dateFrom: String? = null
        var dateTo: String? = null

        fun build(): CallParameters {
            return CallParameters(this)
        }

        fun isActive(isActive: Boolean): Builder {
            this.isActive = isActive
            return this
        }

        fun fixedRate(fixedRate: Boolean): Builder {
            this.fixedRate = fixedRate
            return this
        }

        fun tickerInfo(tickerInfo: String?): Builder {
            this.tickerInfo = tickerInfo
            return this
        }

        fun tickerFrom(tickerFrom: String?): Builder {
            this.tickerFrom = tickerFrom
            return this
        }

        fun tickerTo(tickerTo: String?): Builder {
            this.tickerTo = tickerTo
            return this
        }

        fun txStatus(txStatus: String?): Builder {
            this.txStatus = txStatus
            return this
        }

        fun limit(limit: Int): Builder {
            this.limit = limit
            return this
        }

        fun offset(offset: Int): Builder {
            this.offset = offset
            return this
        }

        fun txId(txId: String?): Builder {
            this.txId = txId
            return this
        }

        fun amount(amount: String?): Builder {
            this.amount = amount
            return this
        }

        fun address(address: String?): Builder {
            this.address = address
            return this
        }

        fun extraId(extraId: String?): Builder {
            this.extraId = extraId
            return this
        }

        fun refundAddress(refundAddress: String?): Builder {
            this.refundAddress = refundAddress
            return this
        }

        fun refundExtraId(refundExtraId: String?): Builder {
            this.refundExtraId = refundExtraId
            return this
        }

        fun userId(userId: String?): Builder {
            this.userId = userId
            return this
        }

        fun payload(payload: String?): Builder {
            this.payload = payload
            return this
        }

        fun contactEmail(contactEmail: String?): Builder {
            this.contactEmail = contactEmail
            return this
        }

        fun includePartners(includePartners: Boolean): Builder {
            this.includePartners = includePartners
            return this
        }

        fun dateFrom(dateFrom: String?): Builder {
            this.dateFrom = dateFrom
            return this
        }

        fun dateTo(dateTo: String?): Builder {
            this.dateTo = dateTo
            return this
        }

    }

    init {
        isActive = builder.isActive
        isFixedRate = builder.fixedRate
        tickerInfo = builder.tickerInfo
        tickerFrom = builder.tickerFrom
        tickerTo = builder.tickerTo
        txStatus = builder.txStatus
        limit = builder.limit
        offset = builder.offset
        txId = builder.txId
        amount = builder.amount
        address = builder.address
        extraId = builder.extraId
        refundAddress = builder.refundAddress
        refundExtraId = builder.refundExtraId
        userId = builder.userId
        payload = builder.payload
        contactEmail = builder.contactEmail
        isIncludePartners = builder.includePartners
        dateFrom = builder.dateFrom
        dateTo = builder.dateTo
    }
}