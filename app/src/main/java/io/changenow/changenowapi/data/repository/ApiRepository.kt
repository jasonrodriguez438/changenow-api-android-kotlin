package io.changenow.changenowapi.data.repository

import io.changenow.api.ChangenowApi
import io.changenow.api.model.*
import io.changenow.api.model.result.*
import io.changenow.api.model.result.CallParameters
import java.text.SimpleDateFormat
import java.util.*

class ApiRepository(private val changenowApi: ChangenowApi) : BaseRepository() {

    /** Common methods  */

    suspend fun <T> currencies(isFixedRate: Boolean) : Result<*>? {
        return changenowApi.apiWrapper<Result<*>>(CallName.CURRENCIES,
            CallParameters.Builder()
                .fixedRate(isFixedRate)
                .build())
    }

    suspend fun <T> currenciesTo(isFixedRate: Boolean) : Result<*>? {
        return changenowApi.apiWrapper<Result<*>>(CallName.CURRENCIES_TO,
            CallParameters.Builder()
                .tickerInfo("btc")
                .fixedRate(isFixedRate)
                .build())
    }

    suspend fun <T> currencyInfo() : Result<*>? {
        return changenowApi.apiWrapper<Result<*>>(CallName.CURRENCY_INFO,
            CallParameters.Builder()
                .tickerInfo("btc")
                .build())
    }

    suspend fun <T> listOfTransactions() : Result<*>? {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        val dateFrom = dateFormat.format(Date(1587556824L))
        val dateTo = dateFormat.format(Date())
        return changenowApi.apiWrapper<Result<*>>(CallName.LIST_OF_TRANSACTIONS,
            CallParameters.Builder()
                .tickerFrom("btc")
                .tickerTo("eth")
                .txStatus("waiting")
                .dateFrom(dateFrom)
                .dateTo(dateTo)
                .build())
    }

    suspend fun <T> txStatus() : Result<*>? {
        return changenowApi.apiWrapper<Result<*>>(CallName.TX_STATUS,
            CallParameters.Builder()
                .txId("51562089402fb6")
                .build())
    }

    suspend fun <T> estimated(isFixedRate: Boolean) : Result<*>? {
        return changenowApi.apiWrapper<Result<*>>(CallName.ESTIMATED,
            CallParameters.Builder()
                .amount("0.003")
                .tickerFrom("btc")
                .tickerTo("eth")
                .fixedRate(isFixedRate)
                .build())
    }

    suspend fun <T> createTx(isFixedRate: Boolean) : Result<*>? {
        return changenowApi.apiWrapper<Result<*>>(CallName.CREATE_TX,
            CallParameters.Builder()
                .tickerFrom("btc")
                .tickerTo("eth")
                .address("0x3f5CE5FBFe3E9af3971dD833D26bA9b5C936f0bE")
                .amount("0.003")
                .fixedRate(isFixedRate)
                .build())
    }

    suspend fun <T> pairs(isFixedRate: Boolean) : Result<*>? {
        return changenowApi.apiWrapper<Result<*>>(CallName.PAIRS,
            CallParameters.Builder()
                .fixedRate(isFixedRate)
                .build())
    }

    suspend fun <T> minAmount() : Result<*>? {
        return changenowApi.apiWrapper<Result<*>>(CallName.MIN_AMOUNT,
            CallParameters.Builder()
                .tickerFrom("btc")
                .tickerTo("eth")
                .build())
    }

}